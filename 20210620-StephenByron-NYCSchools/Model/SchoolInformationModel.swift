//
//  PageResults2.swift
//  20210620-StephenByron-NYCSchools
//
//  Created by Stephen on 6/20/22.
//

import Foundation

struct SchoolInformation: Decodable {
    let dbn: String
    let schoolName: String
    let phoneNumber: String
    let overviewParagraph: String
    let website: String
    var satInfo: SchoolSATInformation?

    enum CodingKeys: String, CodingKey {
        case dbn, satInfo
        case schoolName = "school_name"
        case phoneNumber = "phone_number"
        case overviewParagraph = "overview_paragraph"
        case website = "website"
    }
}


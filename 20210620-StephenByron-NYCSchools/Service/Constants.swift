//
//  NetworkManager1.swift
//  20210620-StephenByron-NYCSchools
//
//  Created by Stephen on 6/20/22.
//

import Foundation

struct Constants {
        static let schoolList = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
        static let satInfo = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn="
    }


//
//  NetworkParams.swift
//  20210620-StephenByron-NYCSchools
//
//  Created by Stephen on 6/20/22.
//

import Foundation

protocol Session {
    func getData(url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> Void)
}

extension URLSession: Session {
    
    func getData(url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> Void) {
        self.dataTask(with: url) { data, response, error in
            completion(data, response, error)
        }.resume()
        
    }
    
}

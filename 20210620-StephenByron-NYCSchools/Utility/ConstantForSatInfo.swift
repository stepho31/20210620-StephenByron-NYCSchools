//
//  ConstantForSatInfo.swift
//  
//
//  Created by Stephen on 6/21/22.
//

import Foundation

struct SatInfo {
    static let noMath = "No Math SAT Scores Obtained"
    static let noWriting = "No Writing SAT Scores Obtained"
    static let noReading = "No Reading SAT Scores Obtained"
    static let noOverview = "No Overview Obtained"
}

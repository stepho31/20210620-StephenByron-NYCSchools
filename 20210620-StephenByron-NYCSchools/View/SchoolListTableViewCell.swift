//
//  AlbumListTableViewCell.swift
//  20210620-StephenByron-NYCSchools
//
//  Created by Stephen on 6/20/22.
//

import UIKit

struct SchoolTableViewCellViewModel {
    let dbn: String?
    let schoolName: String?
    let overviewParagraph: String?
    let numOfSatTestTakers: String?
    let satCriticalReadingAvgScore: String?
    let satMathAvgScore: String?
    let satWritingAvgScore: String?
    let phoneNumber: String?
    let website: String?
    
    enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName = "school_name"
        case overviewParagraph = "overview_paragraph"
        case numOfSatTestTakers = "num_of_sat_test_takers"
        case satCriticalReadingAvgScore = "sat_critical_reading_avg_score"
        case satMathAvgScore = "sat_math_avg_score"
        case satWritingAvgScore = "sat_writing_avg_score"
        case phoneNumber = "phone_number"
        case website = "website"
    
}
}
class SchoolListTableViewCell: UITableViewCell {

    static let reuseId = "\(SchoolListTableViewCell.self)"
    
    lazy var schoolLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        return label
    }()
    lazy var schoolNumberLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        return label
    }()
    lazy var websiteLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        return label
    }()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setUpUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setUpUI() {
        let stackView = UIStackView(frame: .zero)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.spacing = 8
        stackView.backgroundColor = .clear
        
        stackView.addArrangedSubview(self.schoolLabel)
        stackView.addArrangedSubview(self.schoolNumberLabel)
        stackView.addArrangedSubview(self.websiteLabel)
        
        
        self.contentView.addSubview(stackView)
        stackView.bindToSuperView(insets: UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8))
    }
    
    override func prepareForReuse() {
        self.schoolLabel.text = nil
    }
    func configure(schoolName: String?, phoneNumber: String?, website: String?, overviewParagraph: String?) {
        self.schoolLabel.text = "School Name: \(schoolName ?? "???")"
        self.schoolNumberLabel.text = "Phone Number: \(phoneNumber ?? "???")"
        self.websiteLabel.text = "School Email: \(website ?? "???")"
        
    }


}
